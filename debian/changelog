qxw (20200708-2) unstable; urgency=medium

  * Team upload
  * Port to pcre2 (Closes: #999977)

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed
  * Update lintian override info to new format:
    + debian/source/lintian-overrides: line 3
    + debian/qxw.lintian-overrides: line 2
  * Update standards version to 4.6.2, no changes needed

 -- Bastian Germann <bage@debian.org>  Mon, 04 Sep 2023 08:03:58 +0000

qxw (20200708-1) unstable; urgency=medium

  [ Nick Morrott ]
  * New upstream version 20200708 (Closes: #963784, #901661, #758005)
  * d/changelog:
    - Refresh to include only Debian releases
  * d/control:
    - Set Maintainer to Debian Games Team
    - Refresh Vcs-* fields for Debian Games Team
    - Add myself as an Uploader
    - Bump debhelper-compat level to 13
    - Declare compliance with Debian Policy 4.6.0
    - Drop unnecessary greater-than versioned dependencies
    - Update Homepage to HTTPS URL
  * d/copyright:
    - Add Upstream-Contact details
    - Refresh years of upstream copyright
    - Refresh years of Debian copyright
  * d/docs:
    - Drop empty file
  * d/gbp.conf:
    - Add initial gbp config
    - Set upstream-signatures = True
  * d/patches:
    - Drop gcc9.patch (upstream changes)
  * d/qxw.6:
    - Update homepage to HTTPS URL
  * d/rules:
    - Delete boilerplate cruft
    - Add hardening=+all to build
  * d/s/lintian-overrides:
    - We carry the manpage in debian/
    - Upstream's development repository is private
  * d/u/metadata:
    - Add initial upstream metadata
  * d/watch:
    - Migrate to version 4 watch file format

  [ Mark Owen ]
  * d/control:
    - Set Rules-Requires-Root source field to "no"
    - Refresh short and long package descriptions
  * d/copyright:
    - Migrate copyright file to Copyright Format 1.0
  * d/qxw.6:
    - Refresh manpage documentation
  * d/qxw.lintian-overrides:
    - Add override for false positive in source
  * d/watch:
    - Migrate to HTTPS URL for source

 -- Nick Morrott <nickm@debian.org>  Thu, 28 Apr 2022 01:03:11 +0100

qxw (20140331-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix ftbfs with GCC. (Closes: #925816)
    - Thanks Reiner Herrmann.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 16 Mar 2020 20:10:51 +0000

qxw (20140331-1) unstable; urgency=low

  * New upstream release
  * Source signature check

 -- Mark Owen <debian@quinapalus.com>  Tue, 01 Apr 2014 15:11:00 +0100

qxw (20110923-1) unstable; urgency=low

  * New upstream release
  * Minor change to debian/copyright

 -- Mark Owen <debian@quinapalus.com>  Fri, 23 Sep 2011 16:13:18 +0100

qxw (20110818-1) unstable; urgency=low

  * Initial release (Closes: #638465)

 -- Mark Owen <debian@quinapalus.com>  Thu, 18 Aug 2011 14:30:08 +0100
